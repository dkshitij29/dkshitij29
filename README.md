<h1 align="center">Hi 👋, I'm Kshitij</h1>

<p align="left"> <img src="https://komarev.com/ghpvc/?username=dkshitij29&label=Profile%20views&color=0e75b6&style=flat" alt="dkshitij29" /> </p>


- 👀 I’m interested in Open source Projects which are GPLv3 licenced
- 🌱 I’m currently learning Tech-stack(Javascript/Typescript)
- 💞️ I’m looking to collaborate on Open souce Repo's
- 📫 How to reach me on mail:dkshitij@umich.edu

